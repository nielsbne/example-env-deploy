# Example Environment Start Stop using Triggers and Web Only

Setup of the project:

![project merge configuration](project-merge-configuration.png)

To trigger the pipeline:

```sh
INSTALLER_URL=https://gitlab.com/api/v4/projects/16428801/trigger/pipeline
INSTALLER_TOKEN=11fde269f606f5ee8462701ecb6312

ENV=dev
ACTION=deploy # [deploy|undeploy]

curl -X POST "${INSTALLER_URL}" \
     --form "token=${INSTALLER_TOKEN}" \
     --form "ref=${INSTALLER_VERSION:-master}" \
     --form "variables[ACTION]=${ACTION}" \
     --form "variables[ENV]=${ENV}"
```
